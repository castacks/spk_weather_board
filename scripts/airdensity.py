#!/usr/bin/env python
import rospy
from math import exp,log
from spk_weather_board.msg import HumiditySensor, PressureSensor,AirDensityHeight
import sys
import airConversions


class weatherboard(object):
    def __init__(self):
        rospy.Subscriber('/weatherboard/pressure', PressureSensor, self.getPressure)
        rospy.Subscriber('/weatherboard/humidity', HumiditySensor, self.getHumidity)
        self.density_pub = rospy.Publisher('/weatherboard/density',AirDensityHeight)

    def getHumidity(self,data):
        self.humidity = data
    
    def getPressure(self,data):
        try:
            p        = data.pressure_mbar * 0.1 *1000 #PA
            T        = data.temperature_deg # deg
            phi      = self.humidity.humidity_perc /100.0 # %
            T_dew    = self.humidity.dewpoint
            rho_humid = airConversions.airDensity(p,T,phi,T_dew)
            z = airConversions.altitude(p)
            self.density_pub.publish(header=data.header,density_rho=rho_humid,height_m=z,valid_density=True)
        except AttributeError:
            x = 0
        

if __name__ == '__main__':
    rospy.init_node('airdensity')
    j = weatherboard()
    rospy.spin()

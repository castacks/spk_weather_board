#air density conversions module


from math import exp,log


def airDensity(p,T,phi,T_dew):
#specific densitites of air
    R_d      = 287.058 # J/(kg K)
    R_v      = 461.495 # J/(kg K)
    T_kelvin = T + 273.15 # K
    p_sat_exp = 7.5 * T_dew / (T_dew + 237.3);
    p_sat    = 10* 6.1078 * 10**p_sat_exp #kPA
    p_v      = p_sat * phi *1000.0; #PA
    p_d      = p - p_v #PA     
    rho_humid = p_d / (R_d * T_kelvin) + p_v / (R_v * T_kelvin) #kg/m^3
    return rho_humid

def altitude(p):
#altitude calculations
    p_0 =  101325.0 #pa
    T_0 =  288.15 #K
    g   = 9.80665 #m/s^2
    R   = 8.31447 # J/(mol K)
    M   = 0.0289644 # kg/mol
    z = (R * T_0) / (g * M)  * log( p_0/p)
    return z


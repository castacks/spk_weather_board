#!/usr/bin/env python
import rospy

from spektrum.msg import PPM
from spk_weather_board.msg import HumiditySensor, LightBattery, PressureSensor, Pulse, AirDensityHeight
import serial
import sys
import airConversions

DEFAULT_PORT = '/dev/ttyUSB4'

class weatherboard(object):
    def __init__(self,port):
        self.ser = serial.Serial(port, 115200, timeout=1)
        line = self.ser.readline()
        line = self.ser.readline()
        print 'Weatherboard init'
        self.prePub = rospy.Publisher('/weatherboard/pressure', PressureSensor)
        self.humPub = rospy.Publisher('/weatherboard/humidity', HumiditySensor)
        self.lgtPub = rospy.Publisher('/weatherboard/light', LightBattery)
        self.plsPub = rospy.Publisher('/weatherboard/pulse',Pulse)
        self.spkPub = rospy.Publisher('/weatherboard/spektrum',PPM)
        self.density_pub = rospy.Publisher('/weatherboard/density',AirDensityHeight)
    def __del__(self):
        self.ser.close()
    def process(self):
        l = self.ser.readline()
#        rospy.loginfo('RAW: ' + l)
        spl = l.split(':')
        if len(spl) < 2:
            rospy.logwarn('No data received')
            return None
        cc = spl[0].upper()
        if   cc =='H':
            self.humidity = HumiditySensor()
            self.humidity.header=rospy.Header(None,rospy.Time.now(),'/base_frame')
            self.humidity.stamp=int(spl[1])
            self.humidity.humidity_perc=float(spl[2])
            self.humidity.temperature_deg=float(spl[3])
            self.humidity.dewpoint=float(spl[4])
            self.humPub.publish(self.humidity)
        elif cc == 'P':
            self.pressure = PressureSensor()
            self.pressure.header=rospy.Header(None,rospy.Time.now(),'/base_frame')
            self.pressure.stamp=int(spl[1])
            self.pressure.pressure_mbar=float(spl[2])
            self.pressure.temperature_deg=float(spl[3])
            p        = self.pressure.pressure_mbar * 0.1 *1000 #PA
            T        = self.pressure.temperature_deg # deg
            #Now if we have a humidity message also publish air density:
            try:
                phi      = self.humidity.humidity_perc /100.0 # %
                T_dew    = self.humidity.dewpoint
                rho_humid = airConversions.airDensity(p,T,phi,T_dew)
                valid_density = True
            except AttributeError:
                valid_density = False
                rho_humid = 1.0 #Put the default if we have not received a humidity measurement yet
            z = airConversions.altitude(p)
            self.density_pub.publish(header=self.pressure.header,density_rho=rho_humid,height_m=z,valid_density=valid_density)
            self.prePub.publish(self.pressure)
        elif cc == 'L':
            self.lgtPub.publish(header=rospy.Header(None,rospy.Time.now(),'/base_frame'),stamp=int(spl[1]),light_per=float(spl[2]),batt_per=float(spl[3]))
        elif cc == 'S':
            rospy.logerror('TODO. NOT IMPLEMENTED')
        elif cc == 'U':
            self.plsPub.publish(header=rospy.Header(None,rospy.Time.now(),'/base_frame'),stamp=int(spl[1]))
        else:
            rospy.logwarn('Unknown message')
        return

if __name__ == '__main__':
    rospy.init_node('weatherboard')
    port = rospy.get_param('~port', DEFAULT_PORT)
    j = weatherboard(port)
    while not rospy.is_shutdown():
        j.process()
    del j

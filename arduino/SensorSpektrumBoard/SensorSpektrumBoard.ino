  /*
   * rosserial Publisher Example
   * Prints "hello world!"
   */
  //#include <adOS.h>
  // external libraries
#include <FastSerialSpektrum.h>
//FastSerialSpektrumPort0(Serial);
//#include <SpektrumSatellite.h>
//uint32_t spekChannelData[SPEK_MAX_CHANNEL];

 
  #include <SensirionSHTxx.h>
  #include <SFE_BMP085.h> // BMP085 pressure sensor library
  #include <Wire.h> // I2C library (necessary for pressure sensor)
     
  const int XCLR = 9;
  const int EOC = 8;
  const int RF_RTS = 6;
  const int RF_CTS = 5;
  const int STATUSLED = 4;

  
  // analog I/O pins
  const int LIGHT = 7;
  const int BATT_LVL = 6;
  int LED = 0; // status LED
  long baud_rate = 115200; 
  // constant conversion factors
  const float BATT_RATIO = 1.0/63.3271; // divide ADC from BATT_LVL by this to get volts
  const float LIGHT_RATIO = 1.0/10.23;
  // sensor objects
  SensirionSHTxx shtxx(A4,A5);
#define SMP_ERR 999.99
static float taSmp = SMP_ERR;
static float tdSmp = SMP_ERR;
static float rhSmp = SMP_ERR;




  SFE_BMP085 pressure_sensor(BMP_ADDR);
  
  double tempvalue,tempvalue2,pressure;
  
  
//#define MAIN_TASK_PRIO	0
//volatile ados_tcb_t g_mainTcb;
//unsigned char g_mainTaskStack[128]   = {0xE7};

//#define SERVICE_TASK_PRIO5	128
//volatile ados_tcb_t g_serviceTcb5;
//unsigned char g_serviceTaskStack5[96]  = {0xE7};

//#define SERVICE_TASK_PRIO4	127
//volatile ados_tcb_t g_serviceTcb4;
//unsigned char g_serviceTaskStack4[96]  = {0xE7};

//#define SERVICE_TASK_PRIO3	126
//volatile ados_tcb_t g_serviceTcb3;
//unsigned char g_serviceTaskStack3[96]  = {0xE7};


  
  
  void setup()
  {
    Serial.begin(baud_rate);
    // set up inputs and outputs
    pinMode(XCLR,OUTPUT); // output to BMP085 reset (unused)
    digitalWrite(XCLR,HIGH); // make pin high to turn off reset  
    pinMode(EOC,INPUT); // input from BMP085 end of conversion (unused)
    digitalWrite(EOC,LOW); // turn off pullup
    pinMode(STATUSLED,OUTPUT); // output to status LED
    // initialize BMP085 pressure sensor
    if (pressure_sensor.begin() == 0)
      error(1);
       
       
    // set shtxx measurement interval (3-sec)
    // and enable non-blocking
    TWCR &= ~(_BV(TWEN));
    shtxx.begin();
  //  shtxx.setTimerInterval( 500 );
    shtxx.disableTimer();
   
    //Now setup the scheduler:
   // ADOS->Init();
  
    //ADOS->AddTask(&g_serviceTcb5, readLightBattTask,g_serviceTaskStack5, sizeof(g_serviceTaskStack5), SERVICE_TASK_PRIO5);
    //ADOS->AddTask(&g_serviceTcb4, readHumiditySensorTask,g_serviceTaskStack4, sizeof(g_serviceTaskStack4), SERVICE_TASK_PRIO4);
    //ADOS->AddTask(&g_serviceTcb3, readPressureSensorTask,g_serviceTaskStack3, sizeof(g_serviceTaskStack3), SERVICE_TASK_PRIO3);
  //  ADOS->AddTask(&g_mainTcb, readHumiditySensorTask, g_mainTaskStack, sizeof(g_mainTaskStack), MAIN_TASK_PRIO);
  }
  
  void loop()
  {
  
   //readSpektrum();
   readHumiditySensor(); 
   readPressureSensor();
   readLightBatt();
  // delay(10);
  }
  
 
  /*
  void readSpektrum()
  {
    static uint8_t c=0;
    c++;
    if(c>=100)
    {
        c=0;
      if(SpektrumSatellite::fillData(spekChannelData))
	  SpektrumSatellite::printChannels(spekChannelData);
    }
  }*/
  
  void readHumiditySensor()
  {
  static  uint16_t c=100;
  c++;
  if(c>=2000)
  {
    c=0;
  TWCR &= ~(_BV(TWEN));
  
    SHTxxSTATE_SENSOR result;
    shtxx.read(&taSmp, &rhSmp, &tdSmp);
   // if ( result == SHTxxSTATE_COMPLETED ) 
    {
     Serial.printf("H:%d:",millis());Serial.print(rhSmp);Serial.print(":");Serial.print(taSmp);Serial.print(":");Serial.print(tdSmp);Serial.print("\r\n");
    } 
    /*else if ( result == SHTxxSTATE_WRITE_ERROR || result == SHTxxSTATE_TIMEOUT ) 
    {
      taSmp = SMP_ERR;
      tdSmp = SMP_ERR;
      rhSmp = SMP_ERR;
     Serial.printf("H:%d:",millis());Serial.print(rhSmp);Serial.print(":");Serial.print(taSmp);Serial.print(":");Serial.print(tdSmp);Serial.print("\r\n");
    } */
  } 
  }
  
 
  void readPressureSensor()
  {
    TWCR |= _BV(TWEN);  // turn on I2C enable bit so we can access the BMP085 pressure sensor
    static uint8_t c=10;
    c++;
     uint32_t time;
     char status;
     if(c>=10)
     {
       status = pressure_sensor.startTemperature();
      if (status != 0)
        delay(status);
      else
        error(2); 
      // retrieve BMP085 temperature reading
      status = pressure_sensor.getTemperature(&tempvalue); // deg C
      if (status == 0)
        error(3);
      c=0;
     }  
    // start BMP085 pressure reading
    status = pressure_sensor.startPressure(3);
    time = millis();
    if (status != 0)
      delay(status);
    else
      error(4);
    // retrieve BMP085 pressure reading
    status = pressure_sensor.getPressure(&pressure, &tempvalue); // mbar, deg C
   if (status == 0)
      error(5);
    TWCR &= ~(_BV(TWEN));
   
   Serial.printf("P:%d:",time);Serial.print(pressure);Serial.print(":");Serial.print(tempvalue);Serial.print("\r\n");
  }
  

  void readLightBatt()
  {
    static uint8_t c=0;
    c++;
    if(c>=20)
    {
      c=0;
      tempvalue2 = (1023.0 - float(analogRead(LIGHT))) * LIGHT_RATIO; // 0-100 percent
      pressure = float(analogRead(BATT_LVL)) * BATT_RATIO;
      Serial.printf("L:%d:",millis());Serial.print(tempvalue2);Serial.print(":");Serial.print(pressure);Serial.print("\r\n");
    }
  }
  
  void error(int errorcode) // save some space by printing out a generic error message
  {
    Serial.print("E:"); Serial.print(errorcode,DEC); Serial.println("\r\n");
  }
  
 
  /*
   void readSpektrumTask()
  {
    ADOS->Sleep(500);
    for(;;)
    {
 //     readSpektrum();
      ADOS->Sleep(10);
 nh.spinOnce();  
  }
  }
  void readHumiditySensorTask()
  {
    ADOS->Sleep(500);
    for(;;)
    {
      readHumiditySensor();
      ADOS->Sleep(500);
    }
  }
  
    void readLightBattTask()
  {
     ADOS->Sleep(500);
    for(;;)
    {
      readLightBatt();
      ADOS->Sleep(500);
    }
  }
  
   void readPressureSensorTask()
  {
    ADOS->Sleep(500);
    for(;;)
    {
      readHumiditySensor();
      ADOS->Sleep(60);
    }
  }*/


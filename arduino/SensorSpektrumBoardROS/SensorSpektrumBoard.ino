  /*
   * rosserial Publisher Example
   * Prints "hello world!"
   */
  //#include <adOS.h>
  // external libraries
#include <FastSerialSpektrum.h>


FastSerialSpektrumPort0(Serial);

/*
  #include <SpektrumSatellite.h>
 
  #include <SHT1x.h> // SHT15 humidity sensor library
  #include <SFE_BMP085.h> // BMP085 pressure sensor library
  #include <Wire.h> // I2C library (necessary for pressure sensor)
  #include <avr/eeprom.h> // extended EEPROM read/write functions
  #include <ros.h>
  #include <std_msgs/Float32.h>
  #include <std_msgs/UInt16MultiArray.h>
   
  const int XCLR = 9;
  const int EOC = 8;
  const int RF_RTS = 6;
  const int RF_CTS = 5;
  const int STATUSLED = 4;

  
  // analog I/O pins
  const int LIGHT = 7;
  const int BATT_LVL = 6;
  int LED = 0; // status LED
  long baud_rate = 115200; 
  // constant conversion factors
  const float BATT_RATIO = 1.0/63.3271; // divide ADC from BATT_LVL by this to get volts
  const float LIGHT_RATIO = 1.0/10.23;
  // sensor objects
  SHT1x humidity_sensor(A4, A5);
  SFE_BMP085 pressure_sensor(BMP_ADDR);
  
  //Setup all the messages;
  ros::NodeHandle  nh;
  
  std_msgs::Float32 floatMsg;
  //std_msgs::UInt16MultiArray  spektrumMsg;
  double tempvalue,pressure;
  
  ros::Publisher tempHumPub("/as/tpHmDg", &floatMsg);
   ros::Publisher humidityPub("/as/humPrc"  , &floatMsg);
   ros::Publisher tempPrePub("/as/tpPrDg", &floatMsg);
  ros::Publisher pressurePub("/as/prssMBar" , &floatMsg);
  ros::Publisher lightPub("/as/lgt", &floatMsg);
 //ros::Publisher battPub("/as/bttV"     , &floatMsg);
//  ros::Publisher spektrumPub("/as/spektrum"     , &spektrumMsg);
 
  
//#define MAIN_TASK_PRIO	0
//volatile ados_tcb_t g_mainTcb;
//unsigned char g_mainTaskStack[128]   = {0xE7};

//#define SERVICE_TASK_PRIO5	128
//volatile ados_tcb_t g_serviceTcb5;
//unsigned char g_serviceTaskStack5[96]  = {0xE7};

//#define SERVICE_TASK_PRIO4	127
//volatile ados_tcb_t g_serviceTcb4;
//unsigned char g_serviceTaskStack4[96]  = {0xE7};

//#define SERVICE_TASK_PRIO3	126
//volatile ados_tcb_t g_serviceTcb3;
//unsigned char g_serviceTaskStack3[96]  = {0xE7};


  
  
  void setup()
  {
   //nh.getHardware()->setBaud(baud_rate);
   // nh.getHardware()->init();
    // initialize serial port
    //Setup all the sensors:
    // set up inputs and outputs
    pinMode(XCLR,OUTPUT); // output to BMP085 reset (unused)
    digitalWrite(XCLR,HIGH); // make pin high to turn off reset  
    pinMode(EOC,INPUT); // input from BMP085 end of conversion (unused)
    digitalWrite(EOC,LOW); // turn off pullup
    pinMode(STATUSLED,OUTPUT); // output to status LED
    // initialize BMP085 pressure sensor
    if (pressure_sensor.begin() == 0)
      error(1);
    
    //Setup all the ROS components:
    nh.initNode();
    nh.advertise(tempHumPub);
    nh.advertise(humidityPub);

    nh.advertise(tempPrePub);
    nh.advertise(pressurePub);
    nh.advertise(lightPub);
//    nh.advertise(battPub);
//    nh.advertise(spektrumPub);
    
    //Now setup the scheduler:
   // ADOS->Init();
  
    //ADOS->AddTask(&g_serviceTcb5, readLightBattTask,g_serviceTaskStack5, sizeof(g_serviceTaskStack5), SERVICE_TASK_PRIO5);
    //ADOS->AddTask(&g_serviceTcb4, readHumiditySensorTask,g_serviceTaskStack4, sizeof(g_serviceTaskStack4), SERVICE_TASK_PRIO4);
    //ADOS->AddTask(&g_serviceTcb3, readPressureSensorTask,g_serviceTaskStack3, sizeof(g_serviceTaskStack3), SERVICE_TASK_PRIO3);
  //  ADOS->AddTask(&g_mainTcb, readHumiditySensorTask, g_mainTaskStack, sizeof(g_mainTaskStack), MAIN_TASK_PRIO);
  }
  
  void loop()
  {
    //Serial.print("test");
    //delay(1000);
    //readHumiditySensor();
    
   // delay(1000);
   // ADOS->Start();
//   readSpektrum();
   //readHumiditySensor(); 
   readPressureSensor();
   readLightBatt();
    
  // Will not receive messages since we are using this for the spektrum
  nh.spinOnce();
  }
  
 
  
  void readSpektrum()
  {
     //Blocks until we have a frame:
    
    //uint16_t data[8];
    //spektrumMsg.data = data;
    //spektrumMsg.data_length = 8;
   // spektrumPub.publish(&spektrumMsg);
    
  }
  
  void readHumiditySensor()
  {
    TWCR &= ~(_BV(TWEN));  // turn off I2C enable bit so we can access the SHT15 humidity sensor
     floatMsg.data = humidity_sensor.readTemperatureC();
     tempHumPub.publish(&floatMsg);
     floatMsg.data = humidity_sensor.readHumidity();
     humidityPub.publish(&floatMsg);
     
  }
  
 
  void readPressureSensor()
  {
     char status;
     TWCR |= _BV(TWEN);  // turn on I2C enable bit so we can access the BMP085 pressure sensor
    // start BMP085 temperature reading
    status = pressure_sensor.startTemperature();
    if (status != 0)
      delay(status);
      //ADOS->Sleep(status); // if nonzero, status is number of ms to wait for reading to become available
    else
      error(2); 
    // retrieve BMP085 temperature reading
    status = pressure_sensor.getTemperature(&tempvalue); // deg C
    floatMsg.data = tempvalue;
    if (status == 0)
      error(3);
    tempPrePub.publish(&floatMsg);
    // start BMP085 pressure reading
    status = pressure_sensor.startPressure(3);
    if (status != 0)
      delay(status);
      //ADOS->Sleep(status); // if nonzero, status is number of ms to wait for reading to become available
    else
      error(4);
    // retrieve BMP085 pressure reading
    status = pressure_sensor.getPressure(&pressure, &tempvalue); // mbar, deg C
    floatMsg.data = pressure;
    if (status == 0)
      error(5);
    pressurePub.publish(&floatMsg);
   
  }
  

  void readLightBatt()
  {
    floatMsg.data = (1023.0 - float(analogRead(LIGHT))) * LIGHT_RATIO; // 0-100 percent
    lightPub.publish(&floatMsg);
   // floatMsg.data = float(analogRead(BATT_LVL)) * BATT_RATIO;
  //  battPub.publish(&floatMsg);
  
  }
  
  void error(int errorcode) // save some space by printing out a generic error message
  {
    //Serial.print("ERROR #"); Serial.print(errorcode,DEC); Serial.println(", see sketch for cause");
  }
  
 */ 
  /*
   void readSpektrumTask()
  {
    ADOS->Sleep(500);
    for(;;)
    {
 //     readSpektrum();
      ADOS->Sleep(10);
 nh.spinOnce();  
  }
  }
  void readHumiditySensorTask()
  {
    ADOS->Sleep(500);
    for(;;)
    {
      readHumiditySensor();
      ADOS->Sleep(500);
    }
  }
  
    void readLightBattTask()
  {
     ADOS->Sleep(500);
    for(;;)
    {
      readLightBatt();
      ADOS->Sleep(500);
    }
  }
  
   void readPressureSensorTask()
  {
    ADOS->Sleep(500);
    for(;;)
    {
      readHumiditySensor();
      ADOS->Sleep(60);
    }
  }*/
  void setup()
  {
    //Serial.begin(115200);
  }
  void loop()
  {
    //Serial.print("test");
  }

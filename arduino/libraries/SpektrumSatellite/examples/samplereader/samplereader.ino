
#include <FastSerialSpektrum.h>
FastSerialSpektrumPort0(Serial);

#include <SpektrumSatellite.h>

uint32_t spekChannelData[SPEK_MAX_CHANNEL];

void setup(void)
{
        //
        // Set the speed for our replacement serial port.
        //
	Serial.begin(115200);

        //
        // Test printing things
        //
        Serial.print("test\n");
}

void
loop(void)
{
	if(SpektrumSatellite::fillData(spekChannelData))
	  SpektrumSatellite::printChannels(spekChannelData);
	  else
	  Serial.print("nd\n");
}
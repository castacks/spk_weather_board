/*
 * SpectrumReceiver.h
 *
 *  Created on: 9. jan. 2011
 *      Author: develop
 */

#ifndef SPECTRUMSATELLITE_H_
#define SPECTRUMSATELLITE_H_

#define SPEK_MAX_CHANNEL 7

#include "FastSerialSpektrum.h"
#include "Arduino.h"

class SpektrumSatellite{
public:
  static uint8_t fillData(uint32_t spekChannelData[SPEK_MAX_CHANNEL]);
  static void printChannels(uint32_t spekChannelData[SPEK_MAX_CHANNEL]);
};


#endif /* SPECTRUMSATELLITE_H_ */

/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/*
 * SatelliteReceiver.cpp
 *
 *  Created on: 9. jan. 2011
 *      Author: develop
 */
#include "SpektrumSatellite.h"


#define SPEKTRUM 2048

#define SPEK_FRAME_SIZE 16

#if (SPEKTRUM == 1024)
 #define SPEK_CHAN_SHIFT  2       // Assumes 10 bit frames, that is 1024 mode.
 #define SPEK_CHAN_MASK   0x03    // Assumes 10 bit frames, that is 1024 mode.
#endif
#if (SPEKTRUM == 2048)
 #define SPEK_CHAN_SHIFT  3       // Assumes 11 bit frames, that is 2048 mode.
 #define SPEK_CHAN_MASK   0x07    // Assumes 11 bit frames, that is 2048 mode.
#endif


// This should only be use with the fastserialspektrum library since this will line up the receive buffer:


uint8_t SpektrumSatellite::fillData(uint32_t spekChannelData[SPEK_MAX_CHANNEL])
{
  uint8_t serialAv = Serial.available();
  uint8_t rcFrameComplete = serialAv==SPEK_FRAME_SIZE;
  if(rcFrameComplete) 
    {
      Serial.read();
      Serial.read();
      for (uint8_t b = 3; b < SPEK_FRAME_SIZE; b += 2) 
	{
	  uint8_t spekFrameB1 = Serial.read();
	  uint8_t spekFrameB = Serial.read();
	  uint8_t spekChannel = 0x0F & ( spekFrameB1 >> SPEK_CHAN_SHIFT);
	  if (spekChannel < SPEK_MAX_CHANNEL) 
	    {	
	      spekChannelData[spekChannel] = ((uint32_t)(spekFrameB1 & SPEK_CHAN_MASK) << 8) + spekFrameB;
	    }
	}
    }else
    {
      Serial.printf("Av: %d",serialAv);
    }
  return rcFrameComplete;
}

void SpektrumSatellite::printChannels(uint32_t spekChannelData[SPEK_MAX_CHANNEL])
{
  Serial.printf("S:%d",__FastSerialSpektrum__spekTimeLast);
  for(uint8_t i=0;i<SPEK_MAX_CHANNEL;++i)
    {
      Serial.printf(":%d",i,spekChannelData[i]);
    }
  Serial.print("\r\n");
}

/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/*
  BMP085 pressure sensor library
	
  Mike Grusin
  http://www.sparkfun.com

  Uses floating-point equations from the Weather Station Data Logger project
  http://wmrx00.sourceforge.net/
  http://wmrx00.sourceforge.net/Arduino/BMP085-Calcs.pdf

  version 1.2 2012/01/23 update for compatiblity with Arduino 1.0
	
  Example Code:

  #include <SFE_BMP085.h>
  #include <Wire.h>

  SFE_BMP085 pressure(BMP_ADDR);

  char status;
  double T,P,p0,a;

  Serial.begin(9600);
  Serial.println("REBOOT");

  // initialize the sensor (important to get calibration values stored on the device)
  if (pressure.begin())
  {
    Serial.println("BMP085 init success");
    while(1)
    {
      // tell the sensor to start a temperature measurement
      // if request is successful, the number of ms to wait is returned
      // if request is unsuccessful, 0 is returned
      status = pressure.startTemperature();
      if (status != 0)
      {
	// wait for the measurement to complete
	delay(status);
	// retrieve the measurement
	// note that the measurement is stored in the variable T
	// use '&T' to provide the address of T to the function
	// function returns 1 if successful, 0 if failure
	status = pressure.getTemperature(&T);
	if (status != 0)
	{
          // print out the measurement
          Serial.print("temp: ");
          Serial.print(T,2);
          Serial.println(" deg C");

          // tell the sensor to start a pressure measurement
          // the parameter is the oversampling setting, from 0 to 3 (highest res, longest wait)
          // if request is successful, the number of ms to wait is returned
          // if request is unsuccessful, 0 is returned
          status = pressure.startPressure(3);
          if (status != 0)
          {
            // wait for the measurement to complete
            delay(status);
            // retrieve the measurement
            // note that the measurement is stored in the variable P
            // use '&P' to provide the address of P
            // note also that the function requires the previous temperature measurement (T)
            // (if temperature is stable, you can do one temperature measurement for a number of pressure measurements)
            // function returns 1 if successful, 0 if failure
            status = pressure.getPressure(&P,&T);
            if (status != 0)
            {
              // print out the measurement
              Serial.print("pressure: ");
              Serial.print(P,2);
              Serial.println(" mb");

              // the pressure sensor returns abolute pressure, which varies with altitude
              // to remove the effects of altitude, use the sealevel function and your current altitude
              // this number is commonly use in published weather data
              p0 = pressure.sealevel(P,1655.0); // we're at 1655 meters (Boulder, CO)
              Serial.print("sea-level pressure: ");
              Serial.print(p0,2);
              Serial.println(" mb");

              // on the other hand, if you want to determine your altitude from the pressure reading,
              // use the altitude function along with a baseline pressure (sea-level or other)
              a = pressure.altitude(P,p0);
              Serial.print("altitude above sea-level: ");
              Serial.print(a,0);
              Serial.println(" meters");
            }
            else Serial.println("error retrieving pressure measurement\n");
          }
          else Serial.println("error starting pressure measurement\n");
        }
        else Serial.println("error retrieving temperature measurement\n");
      }
      else Serial.println("error starting temperature measurement\n");

      delay(10000);
    }
  }
  else Serial.println("BMP085 init fail\n\n");


*/

#include <SFE_BMP085.h>
#include <Wire.h>
#include <stdio.h>
#include <math.h>

#define OVERSAMPLING 3

SFE_BMP085::SFE_BMP085(char i2c_address)
{
	_i2c_address = i2c_address;
	Wire.begin();
}

char SFE_BMP085::begin()
{

	
	Wire.begin();

	// get calibration data
	
	if (readInt(0xAA,&ac1) &&
		readInt(0xAC,&ac2) &&
		readInt(0xAE,&ac3) &&
		readUInt(0xB0,&ac4) &&
		readUInt(0xB2,&ac5) &&
		readUInt(0xB4,&ac6) &&
		readInt(0xB6,&b1) &&
		readInt(0xB8,&b2) &&
		readInt(0xBA,&mb) &&
		readInt(0xBC,&mc) &&
		readInt(0xBE,&md))
	{
	  /*	  Serial.println("Calibration factors:");
	  Serial.println(ac1);


  Serial.println(ac2);
  Serial.println(ac3);
  Serial.println(ac4);
  Serial.println(ac5);
  Serial.println(ac6);
  Serial.println(b1);
  Serial.println(b2);
  Serial.println(mb);
  Serial.println(mc);
  Serial.println(md);
  Serial.println("Reading AC1 again:");*/
	  //Somehow need to read ac1 twice
	  readInt(0xAA,&ac1);
			
		return(1);
	}
	else
	{
		// error reading calibration data; bad component or connection?
		return(0);
	}
}

//value is address of a short (16-bit) int
char SFE_BMP085::readInt(char address, int *value)
{
	unsigned char data[2];

	data[0] = address;
	if (readBytes(data,2))
	{
		*value = (((int)data[0]<<8)|(int)data[1]);
		//if (*value & 0x8000) *value |= 0xFFFF0000; // sign extend if negative
		return(1);
	}
	*value = 0;
	return(0);
}

//value is address of a short (16-bit) int
char SFE_BMP085::readUInt(char address, unsigned int *value)
{
	unsigned char data[2];

	data[0] = address;
	if (readBytes(data,2))
	{
		*value = (((unsigned int)data[0]<<8)|(unsigned int)data[1]);
		return(1);
	}
	*value = 0;
	return(0);
}

//values is an array of char, first entry should be the register to read from
//subsequent entries will be filled with return values
char SFE_BMP085::readBytes(unsigned char *values, char length)
{
	char x;

	Wire.beginTransmission(_i2c_address);
	Wire.write(values[0]);
	if (Wire.endTransmission() == 0);
	{
		Wire.requestFrom(_i2c_address,length);
		while(Wire.available() != length) ; // wait until bytes are ready
		for(x=0;x<length;x++)
		{
			values[x] = Wire.read();
		}
		return(1);
	}
	return(0);
}

//value is an array of char, first entry should be the register to write to
//subsequent entries will be values to write to that register
char SFE_BMP085::writeBytes(unsigned char *values, char length)
{
	char x;
	
	Wire.beginTransmission(_i2c_address);
	Wire.write(values,length);
	if (Wire.endTransmission() == 0)
		return(1);
	else
		return(0);
}

char SFE_BMP085::startTemperature(void)
{
	unsigned char data[2], result;
	
	data[0] = BMP085_CONTROL_REG;
	data[1] = BMP085_COMMAND_TEMPERATURE;
	result = writeBytes(data, 2);
	if (result) // good write?
		return(5); // return the delay in ms (rounded up) to wait before retrieving data
	else
		return(0); // or return 0 if there was a problem communicating with the BMP
}

char SFE_BMP085::startPressure(char oversampling)
{
	unsigned char data[2], result, delay;
	
	data[0] = BMP085_CONTROL_REG;

	switch (oversampling)
	{
		case 0:
			data[1] = BMP085_COMMAND_PRESSURE0;
			delay = 5;
		break;
		case 1:
			data[1] = BMP085_COMMAND_PRESSURE1;
			delay = 8;
		break;
		case 2:
			data[1] = BMP085_COMMAND_PRESSURE2;
			delay = 14;
		break;
		case 3:
			data[1] = BMP085_COMMAND_PRESSURE3;
			delay = 26;
		break;
		default:
			data[1] = BMP085_COMMAND_PRESSURE0;
			delay = 5;
		break;
	}
	result = writeBytes(data, 2);
	if (result) // good write?
		return(delay); // return the delay in ms (rounded up) to wait before retrieving data
	else
		return(0); // or return 0 if there was a problem communicating with the BMP
}

char SFE_BMP085::getTemperature(double *T)
{
	unsigned char data[2];
	char result;
	double tu, a;
	//char tempstring[20];
	
	data[0] = BMP085_RESULT_REG;

	result = readBytes(data, 2);
	if (result) // good read, calculate temperature
	{
	  RawTemp = data[0];
	  RawTemp = (RawTemp << 8) | data[1];
	  tu = (data[0] * 256.0) + data[1];
	  Calculate();
	  *T = Temp/10.0;
	}
	return(result);
}

// getPressure()
// retrieve and calculate abolute pressure in mbars
// note that parameters are pointers to variables, call with "&var" to send addresses
// requires begin() to have been called once to retrieve calibration parameters
// requires recent temperature reading (startTemperature() / getTemperature()) to calculate pressure
// requires startPressure() to have been called prior to calling getPressure()
// return value will be 1 for success, 0 for I2C error
// note that calculated pressure value is absolute mbars, to compensate for altitude call sealevel()
char SFE_BMP085::getPressure(double *P, double *T)
{
	unsigned char data[3];
	char result;
	
	
	data[0] = BMP085_RESULT_REG;

	result = readBytes(data, 3);
	if (result) // good read, calculate pressure
	{
	  RawPress = (((uint32_t)data[0] << 16) | ((uint32_t)data[1] << 8) | ((uint32_t)data[2])) >> (8 - OVERSAMPLING);
	  Calculate();
	  *P = Press /100.0;
	}
	return(result);
}


// given a pressure P (mb) taken at a specific altitude (meters), return the equivalent pressure (mb) at sea level
double SFE_BMP085::sealevel(double P, double A)
{
	return(P / pow(1-(A/44330.0),5.255));
}

// altitude()
// given a pressure measurement P (mb) and the pressure at a baseline P0 (mb), return altitude (meters) above baseline
double SFE_BMP085::altitude(double P, double P0)
{
	return(44330.0*(1-pow(P/P0,1/5.255)));
}


// Calculate Temperature and Pressure in real units.
void SFE_BMP085::Calculate()
{
	long x1, x2, x3, b3, b5, b6, p;
	unsigned long b4, b7;
	int32_t tmp;

	// See Datasheet page 13 for this formulas
	// Based also on Jee Labs BMP085 example code. Thanks for share.
	// Temperature calculations
	x1 = ((long)RawTemp - ac6) * ac5 >> 15;
	x2 = ((long) mc << 11) / (x1 + md);
	b5 = x1 + x2;
	Temp = (b5 + 8) >> 4;

	// Pressure calculations
	b6 = b5 - 4000;
	x1 = (b2 * (b6 * b6 >> 12)) >> 11;
	x2 = ac2 * b6 >> 11;
	x3 = x1 + x2;
	//b3 = (((int32_t) ac1 * 4 + x3)<<OVERSAMPLING + 2) >> 2; // BAD
	//b3 = ((int32_t) ac1 * 4 + x3 + 2) >> 2;  //OK for OVERSAMPLING=0
	tmp = ac1;
	tmp = (tmp*4 + x3)<<OVERSAMPLING;
	b3 = (tmp+2)/4;
	x1 = ac3 * b6 >> 13;
	x2 = (b1 * (b6 * b6 >> 12)) >> 16;
	x3 = ((x1 + x2) + 2) >> 2;
	b4 = (ac4 * (uint32_t) (x3 + 32768)) >> 15;
	b7 = ((uint32_t) RawPress - b3) * (50000 >> OVERSAMPLING);
	p = b7 < 0x80000000 ? (b7 * 2) / b4 : (b7 / b4) * 2;

	x1 = (p >> 8) * (p >> 8);
	x1 = (x1 * 3038) >> 16;
	x2 = (-7357 * p) >> 16;
	Press = p + ((x1 + x2 + 3791) >> 4);
}
